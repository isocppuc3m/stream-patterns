\section{Stream parallel patterns}

Stream parallel patterns exploit parallelism in the processing of
different items belonging to one or more input data streams. In general, an
input data stream is characterized by having a elements of a given type and
being able to provide items, one after the other, that will be used by some
computation. While those patterns might be seen similar in some cases to existing
traditional data parallel algorithms, a key difference is that neither the full
sequence nor the number of items in the sequence are known in advance.

\subsection{Approaches to manipulating streams}

Two approaches can be considered to manipulate streams in different patterns:

\begin{itemize}

\item A data oriented approach: using data types for input and output streams.

\item A more functional approach, through generation and consumption functions
(or more generally, callable objects).

\end{itemize}

Using a data oriented approach can be achieved by using two types: a type for
representing an input stream and a type for representing an output stream.

\begin{lstlisting}
input_stream<int> s1 = get_input_stream();
output_stream<double> s2 = get_output_stream();

// Sequential processing of stream
while (!s1.finished()) {
  auto x = s1.get();
  auto y = compute(x);
  s2.put(y);
}
\end{lstlisting}

This approach could also be obtained by using types satisfying the concepts of
\cppid{InputIterator} for input streams and \cppid{OutputIterator} for output
streams. However, using iterators requires a mechanism to signal when the input
stream has finished producing data.

A different approach could be representing the source and sink streams as
callable objects. The source stream becomes a callable object returning an item
each time. A protocol needs to be established to signal the end of the stream.
One option would be to return a \cppid{pair} with a \emph{value} and a
\cppkey{bool}. However, a different approach would be to return an
\cppid{optional<value>}, signaling the end of stream with an empty object.
Then, a source stream can be represented by a callable object that produce an
\cppid{optional<value>}, 

\begin{lstlisting}
auto source = [&ifile] {
  if (!ifile) return optional<int>{};
  else {
    int x;
    ifile >> x;
    return make_optional(x);
  }
};
\end{lstlisting}

and a sink stream can be represented by a callable
object that takes an \cppid{optional<value>}.

\begin{lstlisting}
auto sink = [&ofile] (optional<double> v) {
  if (v) ofile << *v:
}

// Sequential processing of stream
for (optional<int> && x = source(); x; x=source()) {
  auto y = compute(*x);
  sink(y);
}
\end{lstlisting}

\subsection{Composing complex patterns}

Being able to compose complex patterns from simpler ones is a highly desirable
characteristic as it allows to express a complex stream computation by nesting
simpler stream computation steps. Moreover, the ability to express a stream pattern
in multiple ways provides the ability to perform transformations on the
computation structures to improve performance~\cite{janjic:2016}.

To be able to compose patterns, we represent separately the idea of stream
source and stream sink. Every top-level pattern may then take those streams to
be able to get input data and store output data. We call those elements
collectively the \emph{bounds} of the computation.

\begin{lstlisting}
bounds b{
  [&infile] { return (!infile)?optional<int>{}:make_optional(read_value(infile)); }
  [&ofile] (int x) { ofile << x << endl; }
};
\end{lstlisting}

Any pattern may take as an argument those bounds to interact with external
streams.

\begin{lstlisting}
patternA(par,
  bounds{
    [&infile] { return (!infile)?optional<int>{}:make_optional(read_value(infile)); }
    [&ofile] (int x) { ofile << x << endl; }
  },
  [] (int x) { return x/2; }
);
\end{lstlisting}
