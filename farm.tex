\section{Farm}

A farm, sometimes also referred as a \emph{task farm} or a \emph{stream map} (in
reference to the functional and data parallel \emph{map} pattern), performs a
transformation on every item coming from an input stream and generates a new
output stream of items. The computations performed during the transformation
are considered fully independent one from the other.

There is a single key parameter to a \emph{farm} pattern:

\begin{itemize}

  \item A single transformation function operating on stream individual data.

\end{itemize}

Parallel execution of a \emph{farm} pattern may be controlled by a number of optional
parameters (with default values):

\begin{itemize}

\item \textbf{Parallelism degree}: Number of parallel threads of execution
performing computations on different elements from the input stream and
generating elements to the output stream.

\item \textbf{Distribution policy}: Policy used to distribute input data items
to computation execution threads. A possible strategy is performing
\emph{round-robin}. Another possible strategy is allowing each free execution thread
to take data items (\emph{auto-scheduling}).

\item \textbf{Granularity}: Number of consecutive items taken by an execution
thread. Appropriate granularity depends highly on the size of individual data
items and the inter-arrival time.

\item \textbf{Ordering}: Specifies if output needs to be ordered or can produce
values in an unordered fashion (with regard to input arrival ordering).

\end{itemize}

Given an input stream with data items of type \cppid{T} and an output stream
with data items of type \cppid{U}. A \emph{farm transformer} is any callable
object \cppid{f} where the statement:

\begin{lstlisting}
T input;
U output = f(input);
\end{lstlisting}

is a valid statement.

Making use of a standalone \emph{farm} pattern requires specifying a source, a
transformer, and a sink.

\begin{lstlisting}
int read_value(istream & is) {
  int x;
  is >> x;
  return x;
}

farm(par, 
  bounds{
    [&infile] { return (!infile)?optional<int>{}:make_optional(read_value(infile)); },
    [&outfile] (double x) { ofile << x << endl; }
  },
  [] (int x) { return 1.0/x; }
);
\end{lstlisting}

A simpler form of \emph{farm} can be specified for the cases where the source
and sink are not specified. Note that this second form is useful only in
compositions.

\begin{lstlisting}
auto f1 = farm(par,
  [] (int x) { return 1.0/x; }
);
\end{lstlisting}

This form is useful in cases where the \emph{farm} will be used inside another
more complex pattern.

\begin{lstlisting}
another_pattern(par,
  do_something,
  farm(par, [] (const vector<int> & v) { return max_element(begin(v), end(v)); },
  do_something_else
);
\end{lstlisting}
