\section{Pipeline}

A \emph{pipeline} performs a computation in several stages. The first stage
takes data items from an input stream. Each stage takes data produced from
previous stage and performs a transformation computation generating data items
for the next stage. All those stages may be performed in parallel.

A \emph{pipeline} takes a number of callable objects:

\begin{itemize}

\item The first callable object is a generator that does not take any argument
and produces data items from first type \cppid{$T_1$}.

\item Every intermediate stage take data items from type \cppid{$T_i$} and generates
data items from type \cppid{$T_{i+1}$}.

\item The last callable object is a consumer that takes data items from type
\cppid{$T_n$}.

\end{itemize}

Given an input stream with data items of type \cppid{$T_0$}, an output stream
of type \cppid{$T_n$}, and a number of intermediate transformation functions
\cppid{$f_i$}, the following expressions are valid:

\begin{lstlisting}[escapechar={@}]
T@$_0$@ x@$_0$@;
T@$_1$@ x@$_1$@ = f@$_1$@(x@$_0$@);
T@$_2$@ x@$_2$@ = f@$_2$@(x@$_1$@);
//...
T@$_n$@ x@$_n$@ = f@$_n$@(x@$_{n-1}$@);
\end{lstlisting}

Making use of a pipeline function requires specifying
a number of intermediate transformation functions and source and sink
callable objects.

\begin{lstlisting}
pipeline(par,
  bounds{
    [&infile] -> optional<frame> { return read_frame(infile); },
    [&outfile] (frame f) { write_frame(outfile, f); }
  },
  [] (frame f) { return filter1(f); },
  [] (frame f) { return filter2(f); }
);
\end{lstlisting}

A second form without source and sink allows composition.

\begin{lstlisting}
farm(par,
  bounds{
    [&infile] -> optional<frame> { return read_frame(infile); },
    [&outfile] (frame f) { write_frame(outfile, f); }
  },
  pipeline(par,
    [] (frame f) { return filter1(f); },
    [] (frame f) { return filter2(f); }
  )
);
\end{lstlisting}
